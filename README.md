# jQuery Task:-

# JQuery Task 1: TAB & ACCORDION

## 1.1 Tab:-

    => Develop a web-page containing three tabs with text.

    => Sample images are attached in the thread.

## 1.2 Accordion:-

    => Develop a web-page containing four accordion sections with text.

    => By default, accordions always keep one section open

    => Click on the currently open section to collapse its content pane

    => Sample images are attached in the thread.

# JQuery Task 2: COUNTDOWN:-

    => Create Countdown mechanism which contains following features. Design reference is added in thread.
    => You can modify/change design but following features must be covered.

## Features:

    => Enter time in seconds and press the start button, the timer will start in decreasing
    manner.

    => Example: For 60:

    => It will start from 00:00:59:99 to 00:00:00:00

    => The “Pause button” will pause the timer and convert the “Start button” into the “Resume
    button”.

    => The “Resume button” will start the timer from the remaining time.

    => The “Stop button” will stop the timer and convert the “Start button” into the “Restart
    button”.

    => The “Restart button” will start the timer again from the beginning.

    => Reset at any stage will set the timer position to 00:00:00:00 and convert the “Resume/Restart button” into the “Start button”.

    => Display a log of the latest activity at the bottom with different colors.

# JQuery Task 3: STOPWATCH

    => Create Stopwatch mechanism which contains following features.

    => You can modify/change design but following features must be covered.

## Features:

    =>Click on the start button to start the stopwatch.

    =>The “Pause button” will pause the timer and convert the “Start button” into the “Resume
    Button”

    =>The “Resume button” will start the timer from the time where we paused.

    =>The “Stop button” will stop the watch and convert the “Start button” into the “Restart button”.

    =>The “Restart button” will start the watch again from the beginning.

    =>Reset at any stage will set the watch position to 00:00:00:00 and convert the “Resume/Restart button” into the “Start button”.

    =>Display a log of the latest activity at the bottom with different colors.

    =>Every time a user hits the PAUSE / STOP button, append the timestamp.

    =>Clear all timestamps when a user hits RESET Button.
